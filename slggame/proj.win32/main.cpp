#include "main.h"
#include "../Classes/AppDelegate.h"
USING_NS_CC;

//
//int APIENTRY _tWinMain(HINSTANCE hInstance,
//                       HINSTANCE hPrevInstance,
//                       LPTSTR    lpCmdLine,
//                       int       nCmdShow)
//{
//    UNREFERENCED_PARAMETER(hPrevInstance);
//    UNREFERENCED_PARAMETER(lpCmdLine);
//
//    // create the application instance
//    AppDelegate app;
//    return Application::getInstance()->run();
//}


int main()
{
    AppDelegate app;
	initAllManager();
    Application::getInstance()->run();
	releaseAllManager();
	return 0;
}


//int main()
//{
//	UnitFactory::InitUnitManager("test.xml");
//}