#pragma once

#include <vector>
#include <map>
#include <iostream>
#include "cocos2d.h"

#define SCAN_UP 0
#define SCAN_DOWN 1
#define SCAN_LEFT 2
#define SCAN_RIGHT 3

using namespace std;
using namespace cocos2d;

class moveAbi;

void initMapInfo(map<Point, int>& mapInfo, int width, int height);

void fillMapInfo(map<Point, int>& mapInfo, cocos2d::experimental::TMXLayer* layer);

void calcMoveRange(map<Point, int>& mapInfo, int moveLimit, vector<moveAbi>& moveResult, Point startPoint);

void calcBattleRange(Point p, vector<moveAbi>& result, int min, int max, map<Point, int>& tileMap);

Point pointToTileMapPoint(cocos2d::experimental::TMXTiledMap* map, Point p);

Point tileMapPointToPoint(cocos2d::experimental::TMXTiledMap* map, Point p);

class AStarPoint
{
public:
	Point point;
	int G;
	int H;
	int getF()
	{
		return G + H;
	}
	AStarPoint* parent;
protected:
private:
};

class AStarFindPath
{
public:
	void init(map<Point, int>&);
	void initOpenArray();
	void initCloseArray();
	vector<Point> findPath(AStarPoint* startPoint, const Point e);
	vector<Point> findPath(Point startPoint, const Point e);

protected:
private:
	bool checkPointInMap(Point p);
	void addOpenArrayAll(AStarPoint* p);
	void addOpenArray(AStarPoint* p, Point distance);
	int calcAStarH(Point p);
	bool checkPointInOpenArray(AStarPoint* p);
	bool checkPointInOpenArray(Point p);
	bool checkPointInCloseArray(AStarPoint* p);
	bool checkPointInCloseArray(Point p);
	void checkRangeMinPointAll(AStarPoint* p);
	void checkRangeMinPoint(AStarPoint* p, Point distance);
	AStarPoint* getPointFromOpenArray(Point p);
	AStarPoint* getMinAStarPoint();
	void movePointToCloseArray(AStarPoint* p);
	map<Point, int> mapInfo;
	vector<AStarPoint*> openArray;
	vector<AStarPoint*> closeArray;
	Point endPoint;
	AStarPoint* nowSelectPoint;
};

class moveAbi
{
public:
	moveAbi(Point p, int m);
	Point point;
	int move;
protected:
private:
};

class battleAbi
{
public:
	battleAbi(Point p1, Point p2);
	Point parent;
	Point point;
};

class Unit;

class Util
{
public:
	static Util* Instance();
	int roll100();
	void calcBattle(Unit* a, Unit* b, int kind);
protected:
	Util();
private:
	static Util* _instance;

};