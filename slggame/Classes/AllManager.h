#pragma once

#include "manager/UnitManager.h"
#include "manager/WeaponManager.h"
#include "battle/BattleUnitManager.h"
#include "manager/JobManager.h"

void initAllManager();

void releaseAllManager();