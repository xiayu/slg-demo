#include "WeaponConfig.h"

#include "tinyxml2/tinyxml2.h"
using namespace tinyxml2;

WeaponConfig* WeaponConfig::_instance = NULL;

WeaponConfig::WeaponConfig()
{
	loadFromXml("testWeaponDict.xml");
}

WeaponConfig* WeaponConfig::Instance()
{
	if (_instance == NULL)
	{
		_instance = new WeaponConfig();
	}
	return _instance;
}


void WeaponConfig::loadFromXml(string path)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.c_str());
	XMLElement* scene = doc.RootElement();
	XMLElement* node = scene->FirstChildElement("dict");
	while (node)
	{
		int kind1 = node->IntAttribute("kind1");
		int kind2 = node->IntAttribute("kind2");
		buffkind.insert(make_pair(Point(kind1, kind2), 1));
		buffkind.insert(make_pair(Point(kind2, kind1), -1));
		node = node->NextSiblingElement();
	}
}

int WeaponConfig::getWeaponBuffKind(Vect& v)
{
	if (buffkind.find(v) != buffkind.end())
	{
		return buffkind[v];
	}
	return 0;
}

void WeaponConfig::Release()
{
	if (_instance != NULL)
	{
		delete _instance;
	}
}

WeaponBuffDict* WeaponBuffDict::_instance = NULL;

WeaponBuffDict* WeaponBuffDict::Instance()
{
	if (_instance == NULL)
	{
		_instance = new WeaponBuffDict();
	}
	return _instance;
}

void WeaponBuffDict::loadFromXml(string path)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.c_str());
	XMLElement* scene = doc.RootElement();
	XMLElement* node = scene->FirstChildElement("buff");
	while (node)
	{
		UnitPro* pro = new UnitPro();
		pro->init(node);
		node = node->NextSiblingElement();
	}
}

WeaponBuffDict::WeaponBuffDict()
{
	loadFromXml("");
}

UnitPro* WeaponBuffDict::getWeaponBuff(Vect& v)
{
	if (buffDict.find(v) == buffDict.end())
	{
		return buffDict[v];
	}
	return NULL;
}

void WeaponBuffDict::Release()
{
	if (_instance != NULL)
	{
		delete _instance;
	}
}