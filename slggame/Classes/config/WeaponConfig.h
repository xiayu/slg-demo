#pragma once
#include <map>
#include "cocos2d.h"
#include "define.h"
#include "UnitPro.h"

using namespace cocos2d;
using namespace std;


class WeaponConfig
{
public:
	static WeaponConfig* Instance();
	int getWeaponBuffKind(Vect& v);
	static void Release();
protected:
	WeaponConfig();
	void loadFromXml(string path);
private:
	static WeaponConfig* _instance;
	map<Vect, int> buffkind; 
};

class WeaponBuffDict
{
public:
	static WeaponBuffDict* Instance();
	UnitPro* getWeaponBuff(Vect& v);
	static void Release();
protected:
	WeaponBuffDict();
	void loadFromXml(string path);
private:
	static WeaponBuffDict* _instance;
	map<Vect, UnitPro*> buffDict;
};