#include "Unit.h"
#include "CCStdC.h"
#include "mapUtil.h"
#include "HelloWorldScene.h"
#include "AllManager.h"

Unit::Unit()
{
	spritePic = bgPic = hpPic = "";
	proNext = NULL;
	weapon = NULL;
}

void Unit::setId(int j)
{
	id = j;
}

void Unit::setJob(JOBS j)
{
	job = j;
}

void Unit::setWeapon(Weapon* w)
{
	weapon = w;
}

void Unit::setUnitPro(UnitPro u)
{
	pro = u;
}

void Unit::setSprite(Sprite* s)
{
	sprite = s;
}

Sprite* Unit::getSprite()
{
	return sprite;
}

void Unit::initLayer()
{
	spriteLayer = Layer::create();
}

void Unit::addUnitToLayer(BattleLayer* n, int zoder, string name)
{
	initSprite();
	spriteLayer->addChild(bgSprite, 100, "hpbg");
	bgSprite->setPosition(0, 100);
	
	spriteLayer->addChild(sprite, 102, "sprite");
	sprite->setPosition(0, 0);
	hpBar = ProgressTimer::create(hpSprite);
	hpBar->setType(ProgressTimer::Type::BAR);

	//������
	hpBar->setMidpoint(Point(0, 0.5));
	hpBar->setBarChangeRate(Point(1, 0));

	hpBar->setPercentage(100);
	hpBar->setPosition(0, 100);
	spriteLayer->addChild(hpBar, 101, "hp");
	n->addChild(spriteLayer, 100);

	sprite->setAnchorPoint(Point(0, 0));
	hpBar->setAnchorPoint(Point(0, 0));
	bgSprite->setAnchorPoint(Point(0, 0));
	setDrawPoint(getPoint(), n->getTileMap());
}

Point Unit::getPoint()
{
	return tileMapPoint;
}

void Unit::setDrawPoint(Point tilePoint, cocos2d::experimental::TMXTiledMap* map)
{
	spriteLayer->setPosition(tileMapPointToPoint(map, tilePoint));
	tileMapPoint = tilePoint;
}

void Unit::initSprite(string s, string hp, string hpbg)
{
	spritePic = s;
	hpPic = hp;
	bgPic = hpbg;
}

int Unit::getId()
{
	return id;
}

void Unit::setPoint(Point point)
{
	tileMapPoint = point;
}

void Unit::initSprite()
{
	initLayer();
	sprite = Sprite::create(spritePic);
	hpSprite = Sprite::create(hpPic);
	bgSprite = Sprite::create(bgPic);
}

UnitProNext* Unit::getProNext()
{
	if (proNext == NULL)
	{
		proNext = createUnitProNext();
	}
	return proNext;
}

UnitProNext* Unit::createUnitProNext()
{
	Job* j = JobManager::Instance()->getJobByKind(job);
	UnitProNext* result = new UnitProNext();
	result->hp = result->hpMax = pro.hpMax + weapon->pro.hpMax;
	result->mpMax = result->mp = pro.mpMax + weapon->pro.mpMax;
	result->atk = pro.sta*j->atkBuff + weapon->pro.atk;
	result->def = pro.def*j->defBuff + weapon->pro.def;
	result->mAtk = pro.mgr*j->mAtkBuff + weapon->pro.mAtk;
	result->mDef = pro.mdf*j->mDefBuff + weapon->pro.mDef;
	result->hit = (pro.skl + pro.lky/2)*j->hitBuff + weapon->pro.hit;
	result->dex = (pro.spd + pro.lky/2)*j->dexBuff + weapon->pro.dex;
	result->cHit = (pro.skl/2)*j->cHitBuff + weapon->pro.cHit;
	result->cDex = pro.lky*j->cDexBuff;
	result->mvl = pro.mvl + weapon->pro.mvl;
	result->maxAtk = weapon->pro.maxAtk;
	result->minAtk = weapon->pro.minAtk;
	return result;
}

Unit::~Unit()
{
	delete proNext;
}

void Unit::PrintUnit()
{
	cout << "id:" << id << endl; 
	getProNext()->print();
}