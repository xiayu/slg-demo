#include "HelloWorldScene.h"
#include "AppMacros.h"
#include <iostream>
#include "AllManager.h"

USING_NS_CC;


Scene* BattleLayer::scene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    BattleLayer *layer = BattleLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    return scene;
}

bool BattleLayer::init()
{
	if ( !Layer::init() )
    {
        return false;
    }
	configAllListener();
	configTileMap();

	//test
	initSpriteFromBattleManager();

	status = NOSELECT;
    return true;
}

void BattleLayer::onTouchesMoved(const std::vector<Touch*>& touches, Event  *event)
{
	
	auto touch = touches[0];
	auto diff = touch->getDelta();

	auto currentPos = getPosition();
	setPosition(currentPos + diff);
}

void BattleLayer::onTouchBegan(const std::vector<Touch*>& touches, Event  *event)
{
	auto t = touches[0]->getLocation();
	t = t - this->getPosition();
	Point p = pointToTileMapPoint(tileMap, t);
	BattleUnitManager* bm = BattleUnitManager::Instance();
	BattleUnitResult* b = bm->findUnitByPoint(p);
	switch (status)
	{
	case NOSELECT:
		{
			if (b->p != UNKNOWN)
			{
				selectUnit = b->u;
				resetAllDrawRange();
				calcMoveRange(mapMoveInfo, selectUnit->getProNext()->mvl, nowDrawMoveRange, p);
				
				drawRange("moveRange", nowDrawMoveRange);
				status = SELECTUNIT;
			}
			break;
		}
		break;
	case UNITTURN:
		break;
	case SELECTUNIT:
		switch (b->p)
		{
		case UNKNOWN:
			if (checkThePointInMoveRange(p))
			{
				selectUnit->setDrawPoint(p, tileMap);		
			}
			resetDrawRange("moveRange", nowDrawMoveRange);
			status = ATKMODE;
			calcBattleRange(selectUnit->getPoint(), nowDrawBattleRange, selectUnit->getProNext()->minAtk, selectUnit->getProNext()->maxAtk, mapMoveInfo);
			drawRange("battleRange", nowDrawBattleRange);
			break;
		default:
			break;
		}
		break;
	case ATKMODE:
		switch (b->p)
		{
		case UNKNOWN:
			resetDrawRange("moveRange", nowDrawMoveRange);
			status =  NOSELECT;
			selectUnit = NULL;
			break;
		case PLAYER_A:
		case PLAYER_B:
		case PLAYER_C:
		case PLAYER_D:
			if (selectUnit == NULL)
			{
				break;
			}
			if (b->u->getId() != selectUnit->getId())
			{
				Util::Instance()->calcBattle(selectUnit, b->u, 0);
			}
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	delete b;
}

void BattleLayer::configRangeLayer(string name)
{
	auto layer = tileMap->getLayer(name);
	auto size = layer->getLayerSize();
	for (int i = 0; i < size.width; i++)
	{
		for (int j = 0; j < size.height; j++)
		{
			auto object = layer->getTileAt(CCPoint(i, j));
			if (object != nullptr)
			{
				object->setVisible(false);
			}
		}
	}	
}

void BattleLayer::configAllRangeLayer()
{
	configRangeLayer("moveRange");
	configRangeLayer("battleRange");
}

void BattleLayer::testAStar()
{
	aStar.init(mapMoveInfo);

	auto result = aStar.findPath(Point(5, 5), Point(-3, -3));

	for (auto i = result.begin(); i != result.end(); i++)
	{
		cout << "AStar: x: " << i->x << " y: " << i->y << endl;
	}
}

void BattleLayer::calcMove()
{
	vector<moveAbi> mapResult;
	calcMoveRange(mapMoveInfo, 3, mapResult, Point(0, 0));
}

void BattleLayer::configTileMap()
{
	tileMap = cocos2d::experimental::TMXTiledMap::create("TileMaps/isometric_grass_and_water.tmx");
	auto metaLayer = tileMap->getLayer("meta");
	metaLayer->setVisible(false);
	initMapInfo(mapMoveInfo, metaLayer->getLayerSize().width, metaLayer->getLayerSize().height);
	fillMapInfo(mapMoveInfo, metaLayer);

	addChild(tileMap, 0, 1);

	configAllRangeLayer();
}

void BattleLayer::configAllListener()
{
	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesBegan = CC_CALLBACK_2(BattleLayer::onTouchBegan, this);
	listener->onTouchesMoved = CC_CALLBACK_2(BattleLayer::onTouchesMoved, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void BattleLayer::drawRange(string name, vector<moveAbi>& range)
{
	Sprite* s = NULL;
	if (range.empty())
	{
		return;
	}
	auto layer = tileMap->getLayer(name);
	for (auto i = range.begin(); i != range.end(); i++)
	{
		s = layer->getTileAt(i->point);
		if (s)
		{
			s->setVisible(true);
		}
	}
}

void BattleLayer::drawAllRange()
{
	drawRange("moveRange", nowDrawMoveRange);
	drawRange("battleRange", nowDrawBattleRange);
}


void BattleLayer::resetDrawRange(string name, vector<moveAbi>& m)
{
	Sprite* s = NULL;
	auto layer = tileMap->getLayer(name);
	for (auto i = m.begin(); i != m.end(); i++)
	{
		s = layer->getTileAt(i->point);
		if (s)
		{
			s->setVisible(false);
		}
	}
	m.swap(vector<moveAbi>());
}

void BattleLayer::resetAllDrawRange()
{
	resetDrawRange("moveRange", nowDrawMoveRange);
	resetDrawRange("battleRange", nowDrawBattleRange);
}

bool BattleLayer::checkThePointInMoveRange(Point p)
{
	for (auto i = nowDrawMoveRange.begin(); i != nowDrawMoveRange.end(); i++)
	{
		if (i->point == p)
		{
			return true;
		}
	}
	return false;
}


void BattleLayer::initSpriteFromBattleManager()
{
	initSpriteByGroup(PLAYER_A);
	initSpriteByGroup(PLAYER_B);
	initSpriteByGroup(PLAYER_C);
	initSpriteByGroup(PLAYER_D);
}

void BattleLayer::initSpriteByGroup(PLAYER p)
{
	BattleUnitManager* bm = BattleUnitManager::Instance();
	auto t = bm->getManagerByGroup(p);
	for (auto i = t.begin(); i != t.end(); i++)
	{
		(*i)->addUnitToLayer(this, 100, "unit");
	}
}

experimental::TMXTiledMap* BattleLayer::getTileMap()
{
	return tileMap;
}

