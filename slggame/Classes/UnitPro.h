#pragma once
#include "define.h"
#include "tinyxml2/tinyxml2.h"

class UnitPro
{
public:
	UnitPro();
	void init(tinyxml2::XMLElement* node);
	UnitPro operator+(UnitPro& u);
	UnitPro operator-(UnitPro& u);
	int hp;//Ѫ��
	int hpMax;//���Ѫ��
	int mp;//ħ��
	int mpMax;//���ħ��
	int sta;//����
	int mgr;//ħ��
	int skl;//����
	int spd;//�ٶ�
	int lky;//����
	int def;//����
	int mdf;//ħ��
	int mvl;//�ƶ���Χ
protected:
private:
};

class UnitProNext
{
public:
	static UnitProNext create(UnitPro, UnitProNext, JOBS, WEAPONRANK);
	UnitProNext();
	UnitProNext operator+(UnitProNext& u);
	UnitProNext operator-(UnitProNext& u);
	void print();
	int hp; //Ѫ��
	int hpMax; //Ѫ������
	int mp; //ħ��ֵ
	int mpMax; //ħ������
	int atk; //������
	int def; //������
	int mAtk; //ħ��������
	int mDef; //ħ��
	int hit; //����
	int dex; //����
	int cHit; // ������
	int cDex; // ��������
	int mvl; //�ƶ���Χ
	int maxAtk; //��󹥻�����
	int minAtk; //��С��������
protected:
private:
};