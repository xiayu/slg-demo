#include "mapUtil.h"
#include "Unit.h"

void initMapInfo(map<Point, int>& mapInfo, int width, int height)
{
	for (int i = 0;  i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			mapInfo[Point(i,j)] = 1; 
		}
	}
}

void fillMapInfo(map<Point, int>& mapInfo, cocos2d::experimental::TMXLayer* layer)
{
	auto cost = layer->getProperty("cost").asInt();
	auto size = layer->getLayerSize();
	for (int i = 0; i < size.width; i++)
	{
		for (int j = 0; j < size.height; j++)
		{
			if (0 != layer->getTileGIDAt(Point(i, j)))
			{
				mapInfo[Point(i, j)] = cost;
			}
		}
	}
}

void scanMapByUp( Point &startPoint, int &moveLimit, map<Point, int>& mapInfo, vector<moveAbi>& moveResult )
{
	startPoint.y--;
	if(mapInfo.find(startPoint) == mapInfo.end())
		return;
	if (moveLimit < mapInfo[startPoint])
	{
		return;
	}else
	{
		moveLimit -= mapInfo[startPoint];
		moveResult.push_back(moveAbi(startPoint, moveLimit));
	}
}

void scanMapByLeft( Point &startPoint, int &moveLimit, map<Point, int>& mapInfo, vector<moveAbi>& moveResult )
{
	startPoint.x--;
	if(mapInfo.find(startPoint) == mapInfo.end())
		return;
	if (moveLimit < mapInfo[startPoint])
	{
		return;
	}else
	{
		moveLimit -= mapInfo[startPoint];
		moveResult.push_back(moveAbi(startPoint, moveLimit));
	}
}

void scanMapByRight( Point &startPoint, int &moveLimit, map<Point, int>& mapInfo, vector<moveAbi>& moveResult )
{
	startPoint.x++;
	if(mapInfo.find(startPoint) == mapInfo.end())
		return;
	if (moveLimit < mapInfo[startPoint])
	{
		return;
	}else
	{
		moveLimit -= mapInfo[startPoint];
		moveResult.push_back(moveAbi(startPoint, moveLimit));
	}
}

void scanMapByDown( Point &startPoint, int &moveLimit, map<Point, int>& mapInfo, vector<moveAbi>& moveResult )
{
	startPoint.y++;
	if(mapInfo.find(startPoint) == mapInfo.end())
		return;
	if (moveLimit < mapInfo[startPoint])
	{
		return;
	}else
	{
		moveLimit -= mapInfo[startPoint];
		moveResult.push_back(moveAbi(startPoint, moveLimit));
	}
}

void scanMapByDirection(map<Point, int>& mapInfo, int moveLimit, Point startPoint, int direction, vector<moveAbi>& moveResult)
{
	if (moveLimit == 0)
	{
		return;
	}
	switch (direction)
	{
	case SCAN_UP:
		scanMapByUp(startPoint, moveLimit, mapInfo, moveResult);
		return;
	case SCAN_LEFT:
		scanMapByLeft(startPoint, moveLimit, mapInfo, moveResult);
		return;
	case SCAN_RIGHT:
		scanMapByRight(startPoint, moveLimit, mapInfo, moveResult);
		return;
	case SCAN_DOWN:
		scanMapByDown(startPoint, moveLimit, mapInfo, moveResult);
		return;
	default:
		break;
	}
}

/*
1.计算方法：应该有2种计算方法，递归与递归展开
递归展开
*/
void calcMoveRange(map<Point, int>& mapInfo, int moveLimit, vector<moveAbi>& moveResult, Point startPoint)
{
	moveResult.push_back(moveAbi(startPoint, moveLimit));
	for (int i = 0; i < moveResult.size(); i++)
	{
		scanMapByDirection(mapInfo, moveResult[i].move, moveResult[i].point, SCAN_UP, moveResult);
		scanMapByDirection(mapInfo, moveResult[i].move, moveResult[i].point, SCAN_DOWN, moveResult);
		scanMapByDirection(mapInfo, moveResult[i].move, moveResult[i].point, SCAN_LEFT, moveResult);
		scanMapByDirection(mapInfo, moveResult[i].move, moveResult[i].point, SCAN_RIGHT, moveResult);
	}
}

void AStarFindPath::init(map<Point, int>& m)
{
	mapInfo = m;
	initOpenArray();
	initCloseArray();
}

void AStarFindPath::initOpenArray()
{
	for (auto i = openArray.begin(); i !=openArray.end(); i++)
	{
		delete *i;
	}
	openArray.swap(vector<AStarPoint*>());
}

void AStarFindPath::initCloseArray()
{
	for (auto i = closeArray.begin(); i != closeArray.end(); i++)
	{
		delete *i;
	}
	closeArray.swap(vector<AStarPoint*>());
}

vector<Point> AStarFindPath::findPath(Point startPoint, const Point e)
{
	AStarPoint* p = new AStarPoint();
	p->point = startPoint;
	p->parent = 0;
	p->G = 0;
	p->H = 0;
	return findPath(p, e);
}

vector<Point> AStarFindPath::findPath(AStarPoint* startPoint, const Point e)
{
	vector<Point> result;
	nowSelectPoint = startPoint;
	openArray.push_back(startPoint);
	endPoint = e;

	while (true)
	{
		nowSelectPoint = getMinAStarPoint();
		if (nowSelectPoint == NULL)
		{
			return result;
		}
		checkRangeMinPointAll(nowSelectPoint);
		if (nowSelectPoint->point == endPoint)
		{
			break;
		}
		addOpenArrayAll(nowSelectPoint);
	}

	while (nowSelectPoint != NULL)
	{
		result.push_back(nowSelectPoint->point);
		nowSelectPoint = nowSelectPoint->parent;
	}
	return result;
}

bool AStarFindPath::checkPointInMap(Point p)
{
	return mapInfo.find(p)!=mapInfo.end();
}

void AStarFindPath::addOpenArrayAll(AStarPoint* p)
{
	addOpenArray(p, Point(1, 0));
	addOpenArray(p, Point(-1, 0));
	addOpenArray(p, Point(0, -1));
	addOpenArray(p, Point(0, 1));

}

void AStarFindPath::addOpenArray(AStarPoint* p, Point distance)
{
	Point temp = p->point - distance;
	if (checkPointInMap(temp) && !checkPointInOpenArray(temp) &&!checkPointInCloseArray(temp))
	{
		AStarPoint* point = new AStarPoint();
		point->parent = p;
		point->point = temp;
		point->H = calcAStarH(temp);
		point->G = p->G + mapInfo[temp];
		openArray.push_back(point);
	}
}

AStarPoint* AStarFindPath::getMinAStarPoint()
{
	AStarPoint* minPoint = NULL;
	auto itor = openArray.begin();
	for (auto i = openArray.begin(); i != openArray.end(); i++)
	{
		if (minPoint == NULL)
		{
			minPoint = *i;
			continue;
		}
		if (minPoint->getF() > (*i)->getF())
		{
			minPoint = *i;
			itor = i;
		}
	}
	if (minPoint != NULL)
	{
		openArray.erase(itor);
		closeArray.push_back(minPoint);
	}
	return minPoint;
}

bool AStarFindPath::checkPointInOpenArray(AStarPoint* p)
{
	return checkPointInOpenArray(p->point);
}

bool AStarFindPath::checkPointInOpenArray(Point p)
{
	for (auto i = openArray.begin(); i != openArray.end(); i++)
	{
		if ((*i)->point == p)
		{
			return true;
		}
	}
	return false;
}

bool AStarFindPath::checkPointInCloseArray(AStarPoint* p)
{
	return checkPointInCloseArray(p->point);
}

bool AStarFindPath::checkPointInCloseArray(Point p)
{
	for (auto i = closeArray.begin(); i != closeArray.end(); i++)
	{
		if ((*i)->point == p)
		{
			return true;
		}
	}
	return false;
}

int AStarFindPath::calcAStarH(Point p)
{
	return abs(p.x - endPoint.x) + abs(p.y - endPoint.y);
}

void AStarFindPath::checkRangeMinPoint(AStarPoint* p, Point distance)
{
	Point temp = p->point - distance;
	if (checkPointInMap(temp) && checkPointInOpenArray(temp))
	{
		auto i = getPointFromOpenArray(temp);
		if (i->getF() < p->getF())
		{
			nowSelectPoint = i;
		}
	}
}

void AStarFindPath::checkRangeMinPointAll(AStarPoint* p)
{
	checkRangeMinPoint(p, Point(1, 0));
	checkRangeMinPoint(p, Point(0, 1));
	checkRangeMinPoint(p, Point(-1, 0));
	checkRangeMinPoint(p, Point(0, -1));
}

AStarPoint* AStarFindPath::getPointFromOpenArray(Point p)
{
	for (auto i = openArray.begin(); i != openArray.end(); i++)
	{
		if ((*i)->point == p)
		{
			return *i;
		}
	}
	return NULL;
}

moveAbi::moveAbi(Point p, int m)
{
	point = p;
	move = m;
}

Point pointToTileMapPoint(cocos2d::experimental::TMXTiledMap* map, Point p)
{
	Point pos = p - map->getPosition();

	cout << "posX: " << pos.x << "posY: " << pos.y << endl;

	auto director = Director::getInstance();

	float size = director->getContentScaleFactor();

	float halfMapWidth = map->getMapSize().width * 0.5;
	float mapHeight = map->getMapSize().height;

	float tileWidth = map->getTileSize().width/size;

	float tileHeight = map->getTileSize().height/size;

	Size visibleSize = Director::getInstance()->getVisibleSize();  

	Point tilePosDiv = Point(pos.x/tileWidth, pos.y/tileHeight);
	float inverseTileY = mapHeight - tilePosDiv.y;

	int posX = (int)(inverseTileY + tilePosDiv.x - halfMapWidth);
	int posY = (int)(inverseTileY - tilePosDiv.x + halfMapWidth);

	posX = MAX(posX, 0);
	posX = MIN(posX, halfMapWidth*2-1);
	posY = MAX(posY, 0);
	posY = MIN(posY, mapHeight-1);

	cout << "posX: " << posX << "posY: " << posY << endl;

	return Point(posX, posY);
}

Point tileMapPointToPoint(cocos2d::experimental::TMXTiledMap* map, Point p)
{
	auto director = Director::getInstance();

	float size = director->getContentScaleFactor();

	float halfMapWidth = map->getMapSize().width * 0.5;
	float mapHeight = map->getMapSize().height;

	float tileWidth = map->getTileSize().width*halfMapWidth/size;

	float tileHeight = map->getTileSize().height*mapHeight/size;

	float posX,posY;
	//X轴变大，实际Y轴变大，X轴变大
	//Y轴变大，实际Y轴变大，X轴变小
	posX = tileWidth + 0.5*(p.x-p.y-1)*map->getTileSize().width/size;
	posY = 0.5*(p.x+p.y+2)*map->getTileSize().height/size;
	posY = tileHeight - posY;
	posX = posX + map->getPosition().x;
	posY = posY + map->getPosition().y;
	cout << "posX:" << posX << " posY:" << posY << endl;
	return Point(posX, posY);
}

battleAbi::battleAbi(Point p1, Point p2)
{
	point = p1;
	parent = p2;
}

void addPointToBattleRange(vector<moveAbi>& result, Point p, map<Point, int>& tileMap)
{
	if (tileMap.find(p) != tileMap.end())
	{
		result.push_back(moveAbi(p, 0));
	}
}

void calcBattleRange(Point p, vector<moveAbi>& result, int min, int max, map<Point, int>& tileMap)
{
	for (int i = min; i <= max; i++)
	{
		int x,y;
		x = i;
		y = 0;
		for (int j = 0; j <= i; j++)
		{
			addPointToBattleRange(result, p + Point(x, y), tileMap);
			addPointToBattleRange(result, p + Point(-x, y), tileMap);
			addPointToBattleRange(result, p + Point(x, -y), tileMap);
			addPointToBattleRange(result, p + Point(-x, -y), tileMap);
			x--;
			y++;
		}
	}
}

Util* Util::_instance = NULL;

Util* Util::Instance()
{
	if (_instance == NULL)
	{
		_instance = new Util();
	}
	return _instance;
}

Util::Util()
{
	 srand((unsigned)time(NULL));
}

int Util::roll100()
{
	return rand()%100;
}

void Util::calcBattle(Unit* a, Unit* b, int kind)
{
	UnitProNext* aPro = a->getProNext();
	UnitProNext* bPro = b->getProNext();

	int damage = 0;
	int hit = aPro->hit - bPro->dex;
	int cHit = aPro->cHit - bPro->cDex;
	if (hit > roll100())
	{
		if (kind = 0)
		{
			damage = aPro->atk - bPro->def;
		}else 
		{
			damage = aPro->mAtk - bPro->mDef;
		}
		if (cHit > roll100())
		{
			damage = damage*2;
		}
	}else
	{
		damage = 0;
	}
	cout << "战斗结果： 战斗ID: " << a->getId() << " vs " << b->getId() << endl;
	cout << "战斗命中率： " << hit << " 战斗暴击率： " << cHit << endl;
	cout << "战斗类型： " << kind << " 战斗伤害 " << damage << endl;
	bPro->hp = bPro->hp - damage;
}