#include "UnitPro.h"
#include <iostream>
using namespace std;

UnitPro::UnitPro()
{
	hp = 0;
	hpMax = 0;
	mp = 0;
	mpMax = 0;
	sta = 0;
	mgr = 0;
	skl = 0;
	spd = 0;
	lky = 0;
	def = 0;
	mdf = 0;
	mvl = 0;
}

void UnitPro::init(tinyxml2::XMLElement* node)
{
	hp = node->IntAttribute("hp");
	hpMax = node->IntAttribute("hpMax");
	mp = node->IntAttribute("mp");
	mpMax = node->IntAttribute("mpMax");
	sta = node->IntAttribute("sta");
	mgr = node->IntAttribute("mgr");
	skl = node->IntAttribute("skl");
	spd = node->IntAttribute("spd");
	lky = node->IntAttribute("lky");
	def = node->IntAttribute("def");
	mdf = node->IntAttribute("mdf");
	mvl = node->IntAttribute("mvl");
}

UnitPro UnitPro::operator+(UnitPro& u)
{
	UnitPro unit;
	unit.hp = this->hp + u.hp;
	unit.hpMax = this->hpMax + u.hpMax;
	unit.mp = this->mp + u.mp;
	unit.mpMax = this->mpMax + u.mpMax;
	unit.sta = this->sta + u.sta;
	unit.mgr = this->mgr + u.mgr;
	unit.skl = this->skl + u.skl;
	unit.spd = this->spd + u.spd;
	unit.lky = this->lky + u.lky;
	unit.def = this->def + u.def;
	unit.mdf = this->mdf + u.mdf;
	unit.mvl = this->mvl + u.mvl;
	return unit;
}

UnitPro UnitPro::operator-(UnitPro& u)
{
	UnitPro unit;
	unit.hp = this->hp - u.hp;
	unit.hpMax = this->hpMax - u.hpMax;
	unit.mp = this->mp - u.mp;
	unit.mpMax = this->mpMax - u.mpMax;
	unit.sta = this->sta - u.sta;
	unit.mgr = this->mgr - u.mgr;
	unit.skl = this->skl - u.skl;
	unit.spd = this->spd - u.spd;
	unit.lky = this->lky - u.lky;
	unit.def = this->def - u.def;
	unit.mdf = this->mdf - u.mdf;
	unit.mvl = this->mvl - u.mvl;
	return unit;
}

UnitProNext::UnitProNext()
{
	hp = 0;
	hpMax = 0;
	mp = 0;
	mpMax = 0;
	atk = 0;
	def = 0;
	mAtk= 0;
	mDef= 0;
	hit= 0;
	dex= 0;
	cHit= 0;
	cDex= 0;
}

UnitProNext UnitProNext::operator+(UnitProNext& u)
{
	UnitProNext unit;
	unit.hp = this->hp + u.hp;
	unit.hpMax = this->hpMax + u.hpMax;
	unit.mp = this->mp + u.mp;
	unit.mpMax = this->mpMax + u.mpMax;
	unit.atk = this->atk + u.atk;
	unit.def = this->def + u.def;
	unit.mAtk = this->mAtk + u.mAtk;
	unit.mDef = this->mDef + u.mDef;
	unit.hit = this->hit + u.hit;
	unit.dex = this->dex + u.dex;
	unit.cHit = this->cHit + u.cHit;
	unit.cDex = this->cDex + u.cDex;
	return unit;
}

UnitProNext UnitProNext::operator-(UnitProNext& u)
{
	UnitProNext unit;
	unit.hp = this->hp - u.hp;
	unit.hpMax = this->hpMax - u.hpMax;
	unit.mp = this->mp - u.mp;
	unit.mpMax = this->mpMax - u.mpMax;
	unit.atk = this->atk - u.atk;
	unit.def = this->def - u.def;
	unit.mAtk = this->mAtk - u.mAtk;
	unit.mDef = this->mDef - u.mDef;
	unit.hit = this->hit - u.hit;
	unit.dex = this->dex - u.dex;
	unit.cHit = this->cHit - u.cHit;
	unit.cDex = this->cDex - u.cDex;
	return unit;
}

void UnitProNext::print()
{
	cout << "hp:" << hp << " mp:" << mp << " atk:" << atk << " def:" << def << " mAtk:" << mAtk << " mDef:" << mDef << " hit:" << hit << " dex:" << dex << " cHit:" << cHit << " cDex:" << cDex << endl;
} 