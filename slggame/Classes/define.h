#pragma once

enum WEAPONKIND
{
	SWORD,
	GUN
};

enum WEAPONRANK
{
	D,
	C,
	B,
	A,
	S
};

enum JOBS
{
	JOB1,
	JOB2
};

enum PLAYER
{
	PLAYER_A,
	PLAYER_B,
	PLAYER_C,
	PLAYER_D,
	UNKNOWN
};

enum GAMESTATUS
{
	SELECTUNIT,
	UNITTURN,
	NOSELECT,
	ATKMODE
};