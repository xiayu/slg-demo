#pragma once
#include "Unit.h"
#include <map>
#include <string>

using namespace std;

class UnitManager
{
public:
	static UnitManager* Instance();
	Unit* getUnitById(int id);
	static void Release();
protected:
	UnitManager();
	~UnitManager();
	void loadFromXml(string path);
private:
	static UnitManager* _instance;
	map<int, Unit*> _template;
};