#include "WeaponManager.h"
#include "tinyxml2/tinyxml2.h"

using namespace tinyxml2;

WeaponManager* WeaponManager::_instance = NULL;

WeaponManager::WeaponManager()
{
	loadFromXml("testWeapon.xml");
}

WeaponManager* WeaponManager::Instance()
{
	if (_instance == NULL)
	{
		_instance = new WeaponManager();
	}
	return _instance;
}

Weapon* WeaponManager::getWeaponById(int id)
{
	return weaponList[id];
}

void WeaponManager::loadFromXml(string path)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.c_str());
	XMLElement* scene = doc.RootElement();
	XMLElement* node = scene->FirstChildElement("weapon");
	while (node)
	{
		Weapon* w = new Weapon();
		w->rank = WEAPONRANK(node->IntAttribute("rank"));
		w->pro.hpMax = node->IntAttribute("hp");
		w->pro.atk = node->IntAttribute("atk");
		w->pro.def = node->IntAttribute("def");
		w->pro.mpMax = node->IntAttribute("mp");
		w->pro.mAtk = node->IntAttribute("mAtk");
		w->pro.mDef = node->IntAttribute("mDef");
		w->pro.hit = node->IntAttribute("hit");
		w->pro.cHit = node->IntAttribute("cHit");
		w->pro.dex = node->IntAttribute("dex");
		w->pro.cDex = node->IntAttribute("cDex");
		w->pro.maxAtk = node->IntAttribute("maxAtk");
		w->pro.minAtk = node->IntAttribute("minAtk");
		w->pro.mvl = node->IntAttribute("mvl");
		int id = node->IntAttribute("id");
		weaponList.insert(make_pair(id, w));
		node = node->NextSiblingElement();
	}
}

void WeaponManager::Release()
{
	if (_instance != NULL)
	{
		delete _instance;
	}
}

WeaponManager::~WeaponManager()
{
	for (auto i = weaponList.begin(); i != weaponList.end(); i++)
	{
		if (i->second != NULL)
		{
			delete i->second;
		}
	}
};