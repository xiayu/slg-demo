#include "JobManager.h"
#include "tinyxml2/tinyxml2.h"

using namespace tinyxml2;

JobManager* JobManager::_instance = NULL;

JobManager* JobManager::Instance()
{
	if (_instance == NULL)
	{
		_instance = new JobManager();
	}
	return _instance;
}

JobManager::JobManager()
{
	loadFromXml("testJob.xml");
}

Job* JobManager::getJobByKind(JOBS j)
{
	return _dict[j];
}

void JobManager::loadFromXml(string path)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.c_str());
	XMLElement* scene = doc.RootElement();
	XMLElement* node = scene->FirstChildElement("job");
	while (node)
	{
		Job* j = new Job();
		j->atkBuff = node->FloatAttribute("atkBuff");
		j->defBuff = node->FloatAttribute("defBuff");
		j->mAtkBuff = node->FloatAttribute("mAtkBuff");
		j->mDefBuff = node->FloatAttribute("mDefBuff");
		j->hitBuff = node->FloatAttribute("hitBuff");
		j->dexBuff = node->FloatAttribute("dexBuff");
		j->cHitBuff = node->FloatAttribute("cHitbuff");
		j->cDexBuff = node->FloatAttribute("cDexBuff");
		j->job = node->Attribute("job");
		JOBS kind = JOBS(node->IntAttribute("kind"));
		_dict.insert(make_pair(kind, j));
		node = node->NextSiblingElement();
	}
}