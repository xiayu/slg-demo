#include "UnitManager.h"
#include "tinyxml2/tinyxml2.h"

using namespace tinyxml2;

UnitManager* UnitManager::_instance = NULL;

UnitManager* UnitManager::Instance()
{
	if (_instance == NULL)
	{
		_instance = new UnitManager();
	}
	return _instance;
}

UnitManager::UnitManager()
{
	loadFromXml("testUnit.xml");
}

Unit* UnitManager::getUnitById(int id)
{
	return _template[id];
}

void UnitManager::loadFromXml(string path)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.c_str());
	XMLElement* scene = doc.RootElement();
	XMLElement* node = scene->FirstChildElement("unit");
	while (node)
	{
		Unit* u = new Unit();
		int id = node->IntAttribute("id");
		UnitPro pro;
		pro.hpMax = node->IntAttribute("hp");
		pro.lky = node->IntAttribute("lky");
		pro.mdf = node->IntAttribute("mdf");
		pro.mgr = node->IntAttribute("mgr");
		pro.mvl = node->IntAttribute("mvl");
		u->setUnitPro(pro);
		int job = node->IntAttribute("job");
		u->setJob(JOBS(job));
		u->setWeapon(NULL);
		string pic = node->Attribute("pic");
		u->initSprite(pic);
		_template.insert(make_pair(id, u));
		node = node->NextSiblingElement();
	}
}

void UnitManager::Release()
{
	if (_instance != NULL)
	{
		delete _instance;
	}
}

UnitManager::~UnitManager()
{
	for (auto i = _template.begin(); i != _template.end(); i++)
	{
		if (i->second != NULL)
		{
			delete i->second;
		}
	}
}