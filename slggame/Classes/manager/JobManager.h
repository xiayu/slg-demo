#pragma once

#include <string>
#include <map>
#include "define.h"

using namespace std;

class Job
{
public:
	string job;
	float atkBuff;
	int defBuff;
	int mAtkBuff;
	int mDefBuff;
	int hitBuff;
	int dexBuff;
	int cHitBuff;
	int cDexBuff;
protected:
private:
};

class JobManager
{
public:
	static JobManager* Instance();
	Job* getJobByKind(JOBS j);
protected:
	JobManager();
	void loadFromXml(string path);
private:
	static JobManager* _instance;
	map<JOBS, Job*> _dict;
};