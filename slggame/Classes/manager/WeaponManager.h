#pragma once

#include "Weapon.h"
#include <map>
#include <string>
using namespace std;

class WeaponManager
{
public:
	static WeaponManager* Instance();
	Weapon* getWeaponById(int id);
	static void Release();
protected:
	WeaponManager();
	~WeaponManager();
	void loadFromXml(string path);
private:
	static WeaponManager* _instance;
	map<int, Weapon*> weaponList;
};