#include "AllManager.h"

void initAllManager()
{
	UnitManager::Instance();
	WeaponManager::Instance();
	BattleUnitManager::Instance();
	JobManager::Instance();
}

void releaseAllManager()
{
	UnitManager::Release();
	WeaponManager::Release();
	BattleUnitManager::Release();
}