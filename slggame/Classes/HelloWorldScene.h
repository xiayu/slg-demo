#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "mapUtil.h"
#include "Unit.h"

using namespace cocos2d;
using namespace std;

class BattleLayer : public cocos2d::Layer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* scene();

	void onTouchBegan(const std::vector<Touch*>& touches, Event  *event);

	void onTouchesMoved(const std::vector<Touch*>& touches, Event  *event);

	experimental::TMXTiledMap* getTileMap();

    // implement the "static node()" method manually
    CREATE_FUNC(BattleLayer);

private:
	void initSpriteFromBattleManager();
	void configRangeLayer(string name);
	void configAllRangeLayer();
	void configTileMap();
	void configAllListener();
	void testAStar();
	void calcMove();
	Unit* checkPointInUnitManager(Point p);
	void drawRange(string name, vector<moveAbi>& range);
	void drawAllRange();
	void resetDrawRange(string name, vector<moveAbi>& m);
	void resetAllDrawRange();
	bool checkThePointInMoveRange(Point p);
	void initSpriteByGroup(PLAYER);
	AStarFindPath aStar;

	map<Point, int> mapMoveInfo;
	vector<moveAbi> nowDrawMoveRange;
	vector<moveAbi> nowDrawBattleRange;

	experimental::TMXTiledMap* tileMap;

	GAMESTATUS status;

	Unit* selectUnit;

private:
	//testʹ��
};

#endif // __HELLOWORLD_SCENE_H__
