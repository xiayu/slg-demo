#pragma once

#include "Weapon.h"
#include "UnitPro.h"
#include "cocos2d.h"
#include <string>

class BattleLayer;

using namespace cocos2d;
using namespace std;
class Unit
{
public:
	Unit();
	int getId();
	void setId(int j);
	void initSprite(string s, string hp="blood.png", string hpbg="blood-1.png");
	UnitProNext* getProNext();
	void setUnitPro(UnitPro u);
	void setJob(JOBS j);
	void setWeapon(Weapon* w);
	void addUnitToLayer(BattleLayer* n, int zoder, std::string name);
	void setDrawPoint(Point tilePoint, cocos2d::experimental::TMXTiledMap* map);
	void setPoint(Point tilePoint);
	Point getPoint();
	void PrintUnit();
	Sprite *getSprite();
	void setSprite(Sprite* s);
	~Unit();
protected:
	UnitProNext* createUnitProNext();//获取二级属性
private:
	int id;
	void initLayer();
	void initSprite();
	UnitProNext* proNext;
	UnitPro pro;
	JOBS job;//职业
	Weapon* weapon;
	Point tileMapPoint;

	Layer* spriteLayer;
	Sprite* sprite;//单位
	Sprite* hpSprite;//血条
	Sprite* bgSprite;//血条背景
	ProgressTimer* hpBar;//血条
	string spritePic;
	string hpPic;
	string bgPic;
};