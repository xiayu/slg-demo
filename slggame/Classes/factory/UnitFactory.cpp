#include "UnitFactory.h"

Unit* UnitFactory::CreateUnit(XMLElement* node)
{
	Unit* u = new Unit();
	return u;
}

Weapon* UnitFactory::CreateWeapon(XMLElement* node)
{
	Weapon* w = new Weapon();
	return w;
}

void UnitFactory::InitUnitManager(string fileName)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(fileName.c_str()); 
	XMLElement *scene=doc.RootElement();  
	XMLElement *surface=scene->FirstChildElement("unit");
	while (surface)
	{
		int i = surface->IntAttribute("id");
		int j = surface->IntAttribute("hp");
		surface=surface->NextSiblingElement();
	}
}