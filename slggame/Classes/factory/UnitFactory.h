#pragma once
#include "Unit.h"
#include "Weapon.h"
#include "tinyxml2/tinyxml2.h"
#include <string>

using namespace tinyxml2;
using namespace std;
class UnitFactory
{
public:
	static Unit* CreateUnit(XMLElement* node);
	static Weapon* CreateWeapon(XMLElement* node);
	static void InitWeaponManager(string fileName);
	static void InitUnitManager(string fileName);
protected:
private:
};