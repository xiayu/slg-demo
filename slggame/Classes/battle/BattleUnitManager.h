#pragma once

#include "Unit.h"
#include <vector>
using namespace std;

class BattleUnitResult
{
public:
	PLAYER p;
	Unit* u;
protected:
private:
};

class BattleUnitManager
{
public:
	static BattleUnitManager* Instance();
	void addUnit(Unit* u, PLAYER player);
	Unit* getUnitByIdAndGroup(int id, PLAYER player);
	BattleUnitResult* findUnitByPoint(Point p);
	void clearAllUnit();
	static void Release();
	vector<Unit*>& getManagerByGroup(PLAYER player);
protected:
	BattleUnitManager();
	~BattleUnitManager();
	void loadFromXml(string path);
	bool searchUnitByPoint(Point p, PLAYER player, BattleUnitResult* b);
private:
	static BattleUnitManager* _instance;
	vector<Unit*> m_Manager_mk_1;
	vector<Unit*> m_Manager_mk_2;
	vector<Unit*> m_Manager_mk_3;
	vector<Unit*> m_Manager_mk_4;
};