#include "../AllManager.h"
#include "tinyxml2/tinyxml2.h"

using namespace tinyxml2;

BattleUnitManager* BattleUnitManager::_instance = NULL;

BattleUnitManager* BattleUnitManager::Instance()
{
	if (_instance == NULL)
	{
		_instance = new BattleUnitManager();
	}
	return _instance;
}

vector<Unit*>& BattleUnitManager::getManagerByGroup(PLAYER player)
{
	switch (player)
	{
	case PLAYER_A:
		return m_Manager_mk_1;
	case PLAYER_B:
		return m_Manager_mk_2;
	case PLAYER_C:
		return m_Manager_mk_3;
	case PLAYER_D:
		return m_Manager_mk_4;
	default:
		break;
	}
}

void BattleUnitManager::addUnit(Unit* u, PLAYER player)
{
	vector<Unit*>& manager = getManagerByGroup(player);
	manager.push_back(u);
}

Unit* BattleUnitManager::getUnitByIdAndGroup(int id, PLAYER player)
{
	vector<Unit*> manager = getManagerByGroup(player);
	for (auto i = manager.begin(); i != manager.end(); i++)
	{
		if (id == (*i)->getId())
		{
			return *i;
		}
	}
	return NULL;
}

BattleUnitManager::BattleUnitManager()
{
	loadFromXml("testBattleUnit.xml");
}

BattleUnitResult* BattleUnitManager::findUnitByPoint(Point p)
{
	Unit* u = NULL;
	BattleUnitResult* b = new BattleUnitResult();
	b->u = u;
	b->p = PLAYER::UNKNOWN;
	if (searchUnitByPoint(p, PLAYER_A, b))
		return b;
	if (searchUnitByPoint(p, PLAYER_B, b))
		return b;
	if (searchUnitByPoint(p, PLAYER_C, b))
		return b;
	if (searchUnitByPoint(p, PLAYER_D, b))
		return b;
	return b;
}

bool BattleUnitManager::searchUnitByPoint(Point p, PLAYER player, BattleUnitResult* b)
{
	auto m = getManagerByGroup(player);
	for (auto i = m.begin(); i != m.end(); i++)
	{
		if((*i)->getPoint() == p)
		{
			b->p = player;
			b->u = (*i);
			return true;
		}
	}
	return false;
}

void BattleUnitManager::Release()
{
	if (_instance != NULL)
	{
		delete _instance;
	}
}

BattleUnitManager::~BattleUnitManager()
{

}

void BattleUnitManager::loadFromXml(string path)
{
	UnitManager* um = UnitManager::Instance();
	WeaponManager* wp = WeaponManager::Instance();
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.c_str());
	XMLElement* scene = doc.RootElement();
	XMLElement* node = scene->FirstChildElement("unit");
	while (node)
	{
		int id = node->IntAttribute("id");
		int group = node->IntAttribute("group");
		int weapon = node->IntAttribute("weapon");
		int x = node->IntAttribute("x");
		int y = node->IntAttribute("y");
		int gameid = node->IntAttribute("gameid");
		Unit* u = um->getUnitById(id);
		u->setId(gameid);
		u->setPoint(Point(x,y));
		Weapon* w = wp->getWeaponById(weapon);
		u->setWeapon(w);
		auto& manager = getManagerByGroup(PLAYER(group));
		manager.push_back(u);
		node = node->NextSiblingElement();
	}
}